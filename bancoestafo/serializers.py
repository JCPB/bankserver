#from .models import Estacionamiento,Comuna
from django.contrib.auth.models import User
from .models import Cliente
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('pk','url', 'username', 'email', 'is_staff')

class ClienteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cliente
        fields = ('pk','url', 'nombre', 'apellido', 'email', 'rut')