from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Cliente
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import authentication_classes
from django.http import JsonResponse,HttpResponse

from rest_framework import viewsets,generics
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response
from .serializers import UserSerializer, ClienteSerializer
from django.core import serializers
# Create your views here.

def index(request):
    return render(request,"inicio.html",{})

@csrf_exempt
def addclient(request):
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    rut = request.POST.get('rut','')
    correo = request.POST.get('email','')
    cli = Cliente(nombre=nombre,apellido=apellido,rut=rut,email=correo)
    cli.save()
    return HttpResponse("<script>alert('anadido')</script>")

# rest
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

@api_view(['post'])
@authentication_classes((SessionAuthentication, BasicAuthentication,TokenAuthentication))
@permission_classes((IsAuthenticated,))
def clientespost(request):
    clientes = Cliente.objects.all()
    jsonresult = [ob.as_json() for ob in clientes]
    return JsonResponse({'clientes':jsonresult})

@api_view(['get'])
@csrf_exempt
@permission_classes((AllowAny,))
def clientesu(request):
    clientes = Cliente.objects.all()
    jsonresult = [ob.as_json() for ob in clientes]
    return JsonResponse({'clientes':jsonresult})

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    success = False
    username = request.data.get("username")
    password = request.data.get("password")
    reason = ''
    if username is None or password is None:
        return Response({'success':success,'reason': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'success':success,'reason': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    else:
        usuario = User.objects.get(username=username)
    token, _ = Token.objects.get_or_create(user=user)
    success = True
    reason = 'logged'
    return Response({'success':success,'reason':reason, 'token': token.key , 'username': user.username, 'lastlogin':user.last_login}, status=HTTP_200_OK)