from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Cliente(models.Model):
    rut = models.CharField(max_length = 12)
    nombre = models.CharField(max_length = 35)
    apellido = models.CharField(max_length = 35)
    email = models.CharField(max_length = 35)
    def as_json(self):
        return dict(
            rut = self.rut,
            nombre = self.nombre,
            apellido = self.apellido,
            email = self.email)