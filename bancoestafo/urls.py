from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path,include
from . import views

from django.conf.urls import url
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'clientes', views.ClienteViewSet)

urlpatterns = [
    path('',views.index,name='index'),
    path('clientespost',views.clientespost,name='clientespost'),
    path('clientesu',views.clientesu,name='clientesu'),
    path('api/login', views.login),
    path('api/login/', views.login),
    path('addclient', views.addclient, name='addclient'),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^', include(router.urls))
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
